CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

The Vue JS quick start intends to integrate vueJS component easily to drupal CMS using JSON API. This module is provided component based implementation instead of stand alone Vue JS Application.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/quick_start_vuejs_component

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/search/quick_start_vuejs_component


REQUIREMENTS
------------

No special requirements.


RECOMMENDED MODULES
-------------------

 * Node.js should be install during development:
    Please visit (https://nodejs.org/en/download/) to install Node.js

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module.
   See: https://www.drupal.org/node/895232 for further information.

 * Visit "vueapp" folder and execute below commands to install Vue App.
   npm install

 * Shell script(vueappbuild.sh):
   used to build vueApp and update JS build libraries app.js and chunk-vendors.js.
   It is one time command to build the app.
  


CONFIGURATION
-------------

 * No admin configuration.


MAINTAINERS
-----------

Current maintainers:
 * Harshit Thakore (harshit) - https://www.drupal.org/u/harshit97
 * Dimple Limbad (dimple) - https://www.drupal.org/u/dimplel
 * Maitri Gandhi (maitri) - https://www.drupal.org/u/maitrigandhi

This project has been sponsored by:
 * KudosIntech Software Pvt. Ltd.
   KudosIntech was established in 2011 with the goal of eliminating the idea of One-Size-Fits-All Technology. 
   Visit: https://www.kudosintech.com/ for more information.