rm -r dist
echo "Checking syntax error"
npm run lint
echo "Building Final App"
npm run build
cd dist/js
mv chunk-vendors.*.js chunk-vendors.js
mv app.*.js app.js
# mv chunk-vendors.*.js.map chunk-vendors.js
# mv app.*.js.map app.js

