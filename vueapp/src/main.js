import { createApp } from 'vue'
import App from './App.vue'
import MainVuePage from './components/MainVuePage';

createApp(App).mount('#app')
createApp(MainVuePage).mount('#news')